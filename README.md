# docker-phonetisaurus

Author: Ewald van der Westhuizen

Affiliation: Stellenbosch University


This simple docker image is for hosting Phonetisaurus.


## Install Docker

1. Follow the Docker installation instructions under __Install using a repository__ at: https://docs.docker.com/engine/install/ubuntu/#install-using-the-repository
2. Then follow the instructions under __Manage Docker as a non-root user__ at: https://docs.docker.com/engine/install/linux-postinstall/#manage-docker-as-a-non-root-user


## Building the image

The command format to execute a build is:
```
docker build -t <an_appropriate_name_for_image> <path/to/the/Dockerfile/directory>
```

To build an image that will be used with the Stellenbosch ASR systems, use `phonetisaurus_ubuntu` as the image name, as in:
```
docker build -t phonetisaurus_ubuntu ./
```

If you are __not__ inside the Stellenbosch University network, comment out lines 2 and 3 in the Dockerfile, as in:
```
#RUN sed -i 's/archive\.ubuntu\.com/ftp.sun.ac.za/g' /etc/apt/sources.list
#RUN sed -i 's/security\.ubuntu\.com/ftp.sun.ac.za/g' /etc/apt/sources.list
```


## Usage

To run temporarily:
```
docker run -v /path/to/directory/containing/models/on/host/pc:/root/models -it an_appropriate_name_for_image
```

A more permanent setup is to create a permanent container and then start it up as required:
```
docker create -v /path/to/directory/containing/models/on/host/pc:/root/models -it --name a_name_for_container the_image_name
docker start a_name_for_container
docker exec -it a_name_for_container bash
```

