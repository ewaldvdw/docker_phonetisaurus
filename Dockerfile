FROM ubuntu:bionic
RUN sed -i 's/archive\.ubuntu\.com/ftp.sun.ac.za/g' /etc/apt/sources.list
RUN sed -i 's/security\.ubuntu\.com/ftp.sun.ac.za/g' /etc/apt/sources.list
RUN apt-get update
RUN DEBIAN_FRONTEND=noninteractive; apt-get install -y apt-utils less
RUN apt-get update
RUN apt-get install -y git g++ autoconf-archive make libtool
RUN apt-get install -y python-setuptools python-dev
RUN apt-get install -y gfortran
RUN apt-get install -y wget
RUN apt-get -y clean
# Install OpenFst
WORKDIR "/root"
RUN wget http://www.openfst.org/twiki/pub/FST/FstDownload/openfst-1.6.2.tar.gz
RUN tar -xvzf openfst-1.6.2.tar.gz && cd openfst-1.6.2
WORKDIR "/root/openfst-1.6.2"
RUN ./configure --enable-static --enable-shared --enable-far --enable-ngram-fsts
RUN make -j 4
RUN make install
WORKDIR "/root"
# Extend your LD_LIBRARY_PATH .bashrc:
RUN echo 'export LD_LIBRARY_PATH=${LD_LIBRARY_PATH}:/usr/local/lib:/usr/local/lib/fst' >> ~/.bashrc
RUN rm -rf "/root/openfst-1.6.2" openfst-1.6.2.tar.gz
RUN git clone https://github.com/AdolfVonKleist/Phonetisaurus.git
WORKDIR "/root/Phonetisaurus"
RUN ./configure
RUN make
RUN make install
WORKDIR "/root"
RUN rm -rf "/root/Phonetisaurus"
COPY ./run_g2p_fr.sh ./
RUN chmod u+x run_g2p_fr.sh
CMD /bin/bash

